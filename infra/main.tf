# Declara a versão mínima do provedor AWS necessária para este código Terraform
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
  
}
# Define o provedor AWS e especifica a região onde as operações serão realizadas
provider "aws" {
  profile = "default"
  region  = var.região_aws
  }
# Resource para criar uma instância AWS usando uma AMI específica
resource "aws_instance" "code_iac" {
  ami          = var.ami
  instance_type = var.instancia
  key_name      = var.chave
  tags = {
    Name = "${var.iac_name}"
  }
}
# Recurso para buscar a chave SSH publica criada
resource "aws_key_pair" "chaveSSH" {
  key_name = var.chave
  public_key = file("${var.chave}.pub")
}
# Retornar o Ipv4 quando a Vm é aplicada na AWS
output "IP_plub" {
  value = aws_instance.code_iac.public_ip
}