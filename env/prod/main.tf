module "amb_prod" {
  source = "../../infra"
  instancia = "t2.micro"
  chave = "iac_key_prod"
  região_aws = "us-east-2"
  ami = "ami-08cba41c585e4a2e2"
  iac_name = "iac_amb_prod"
}
output "IP" {
  value = module.amb_prod.IP_plub
}