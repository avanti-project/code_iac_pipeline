module "amb_dev" {
  source = "../../infra"
  instancia = "t2.micro"
  chave = "iac_key_dev"
  região_aws = "us-east-2"
  ami = "ami-0e83be366243f524a"
  iac_name = "iac_amb_dev"
}
output "IP" {
  value = module.amb_dev.IP_plub
}